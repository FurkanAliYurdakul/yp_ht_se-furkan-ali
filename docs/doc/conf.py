# Copyright (C) ALbert Mietus; 2020. Part of YP_HT_SE training
# -*- coding: utf-8 -*-

# read STD config ...
#==========================================
import sys; sys.path.append('../_std_settings/conf')
from std_conf import *

# General information about the project.
#======================================
project = 'YP_HT_SE/2000'
copyright = "ALbert Mietus e.o, 2020"

show_authors = True

# Overrule std_conf, where needed
#================================
from datetime import datetime
version = datetime.now().strftime("%Y%m%d.%H")

release = version
html_title = project + " | " + release # DEFAULT: '<project> v<revision> documentation' -- Strip "documentation"


html_context = {
    'extra_css_files': ['_static/custom.css'],
}


